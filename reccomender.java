package reccomender;

import java.io.*;
import java.util.*;
import reccomender.objects.book;
import reccomender.objects.user;
import reccomender.objects.result;

public class reccomender {
    final String ratings_csv = "/media/nika/F8300A8C300A51D4/Nika/Sapienza_Data_Science/ADM/BX-CSV-Dump/BX-Book-Ratings.csv";
    final String books_csv = "/media/nika/F8300A8C300A51D4/Nika/Sapienza_Data_Science/ADM/BX-CSV-Dump/BX-Books.csv";
    final String users_csv = "/media/nika/F8300A8C300A51D4/Nika/Sapienza_Data_Science/ADM/BX-CSV-Dump/BX-Users.csv";
    final String new_user_ratings_csv = "/media/nika/F8300A8C300A51D4/Nika/Sapienza_Data_Science/ADM/BX-CSV-Dump/new_user.csv";
    final String recommendeds_csv = "/media/nika/F8300A8C300A51D4/Nika/Sapienza_Data_Science/ADM/BX-CSV-Dump/recommended_books.csv";
    final int min_rating_score = 0;
    private int document_length;
    final ArrayList<user> users = new ArrayList<>();
    ArrayList<String[]> hidden_ratings = new ArrayList<>();
    double maximum_final_rating = 0;
    double minimum_final_rating = 10000;
    double rmse = 0;
    
    
    public result find_recommended_books(){
        result result = new result();
        try {
            this.program_type("online"); // online or offline
            this.find_users_rates(ratings_csv); // for each user retrieve all rated books
            this.fill_books_informations(books_csv); // for each user fill information of books he has rated
            this.prepare_for_tf_idf(); // retrieve all information necessarry for tf-idf calculations
            this.calculate_tf_idfs(); 
            this.generate_users_profiles(); 
            this.find_books_to_recommend(10); 
            this.calculate_rmses(); 
            this.write_recommends_in_csv();
        } catch (Exception ex) {
            result.error_occured = true; result.error_long_message = ex.getMessage();
        }
        return result;
    }
    
    public void program_type(String type) throws Exception{
        if(type.equals("online")) {
            this.prepare_system_for_online_version("111111"); // 111111 - user id
        } else if(type.equals("offline")){
            this.prepare_system_for_offline_version(10); // 10 - number of users
        } else {
            throw new Exception("You should choose either online or offline version");
        }
    }
    
    public void prepare_system_for_online_version(String user_id) throws Exception{
        user user = new user(user_id);
        users.add(user);
        this.add_new_user_ratings_to_csv();
    }
    
    public void prepare_system_for_offline_version(int number_of_users) throws Exception{
        this.create_users_list(number_of_users);
    }
    
    public void find_users_rates(String ratings_csv) throws Exception{
        BufferedReader reader = new BufferedReader(new FileReader(ratings_csv));
        reader.readLine();
        String line;
        while((line = reader.readLine()) != null){
            String[] fields = line.split(";");
            outerloop:
            for (int i = 0; i < this.users.size(); i++) {
                if(Integer.parseInt(fields[0].replace("\"", "")) == users.get(i).id && Integer.parseInt(fields[2].replace("\"", "")) > min_rating_score){
                    for (int j = 0; j < this.hidden_ratings.size(); j++) {
                        if(this.users.get(i).id == Integer.parseInt(this.hidden_ratings.get(j)[0].replace("\"", "")) && fields[1].replace("\"", "").equals(this.hidden_ratings.get(j)[1].replace("\"", ""))) 
                        {
                            int a = 0;
                            continue outerloop;
                        }
                    }
                    int a = Integer.parseInt(fields[2].replace("\"", ""));
                    String b = fields[1];
                    users.get(i).isbn_score.put(fields[1], Integer.parseInt(fields[2].replace("\"", "")));
                }
            }
        }
    }
    
    public void fill_books_informations(String books_csv) throws Exception {
        BufferedReader reader = new BufferedReader(new FileReader(books_csv));
        reader.readLine();
        String line; int counter = 0;
        while((line = reader.readLine()) != null){
            try{
            line = line.replace("&amp;", "");
            String[] fields = line.split(";");
            for (int i = 0; i < this.users.size(); i++) {
                for (Map.Entry<String, Integer> entry : this.users.get(i).isbn_score.entrySet()) {
                    if(entry.getKey().equals(fields[0])){
//                        System.out.println(fields[0] + "|" +fields[1] + "|"+ fields[2] + "|"+ fields[3] + "|"+ fields[4] + "|"+ entry.getValue());
                        book book = new book(fields[0], fields[1], fields[2], fields[3], fields[4], entry.getValue());
                        book.generate_profile_items(); // Create string of profile items
                        book.fill_appearance_placeholders(); // Placeholders for maximum and total appearences of words(features)
                        this.users.get(i).books.add(book);
                    }
                }
            }
            counter ++;
            } catch(Exception ex){ 
            int a = 0;
            }
        }
        this.document_length = counter;
    }
    
    public void prepare_for_tf_idf() throws Exception{
        BufferedReader reader = new BufferedReader(new FileReader(books_csv));
        reader.readLine();
        String line;
        while((line = reader.readLine()) != null){
            String[] fields = line.split(";");
            String book_words = (fields[1] + " " + fields[2] + " " + fields[4]).replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase(); // this takes a lot of time
            List arr = Arrays.asList(book_words.split(" "));
            
            for (int k = 0; k < this.users.size(); k++) {
                for (int i = 0; i < this.users.get(k).books.size(); i++) {
                    String[] words = this.users.get(k).books.get(i).profile_items.split(" ");
                    for (int j = 0; j < words.length; j++) {
                        if(arr.contains(words[j])){ 
                            this.users.get(k).books.get(i).total_appearences.set(j, this.users.get(k).books.get(i).total_appearences.get(j) + 1);
                        }

                        int word_appearance = Collections.frequency(arr, words[j]); // this takes a lot of time
                        if(word_appearance > this.users.get(k).books.get(i).maximum_appearences.get(j)){
                            this.users.get(k).books.get(i).maximum_appearences.set(j, word_appearance);
                        }
                    }
                }
            }
            
        }
        for (int k = 0; k < this.users.size(); k++) {
            for (int i = 0; i < this.users.get(k).books.size(); i++) {
                List arr = Arrays.asList(this.users.get(k).books.get(i).profile_items.split(" "));
                String[] words = this.users.get(k).books.get(i).profile_items.split(" ");
                for (int j = 0; j < words.length; j++) {
                    int word_appearance = Collections.frequency(arr, words[j]);
                    if(word_appearance > this.users.get(k).books.get(i).in_document_appearence.get(j)){
                            this.users.get(k).books.get(i).in_document_appearence.set(j, word_appearance);
                    }
                }
            }
        }
    }
    
    public void calculate_tf_idfs() {
        for (int k = 0; k < this.users.size(); k++) {
            for (int i = 0; i < this.users.get(k).books.size(); i++) {
                book book = this.users.get(k).books.get(i);
                String[] words = book.profile_items.split(" ");
                for (int j = 0; j < words.length; j++) {
                    double tf = (double)book.in_document_appearence.get(j) / (double)book.maximum_appearences.get(j);
                    double idf = Math.log10((double)this.document_length / (double)book.total_appearences.get(j));
                    Double tf_idf = tf * idf * book.user_score;
                    book.tf_idfs.add(tf_idf);
                }
            }
        }
    }
    
    public void generate_users_profiles(){
        for (int k = 0; k < this.users.size(); k++) {
            this.users.get(k).user_profile = new HashMap<String,Double>();
            for (int i = 0; i < this.users.get(k).books.size(); i++) {
                for (int j = 0; j < this.users.get(k).books.get(i).tf_idfs.size(); j++) {
                    Double score = this.users.get(k).books.get(i).tf_idfs.get(j);
                    String feature = this.users.get(k).books.get(i).profile_items.split(" ")[j];
                    String key = this.users.get(k).index_of_feature(feature);
                    if(key != ""){
                        Double new_value = this.users.get(k).user_profile.get(key) * score / 2;
                        this.users.get(k).user_profile.put(key, new_value);
                    } else { this.users.get(k).user_profile.put(feature, score); }
                }
            }
        }
    }
    
    public void find_books_to_recommend(int number_of_reccomends) throws Exception{
        for (int k = 0; k < this.users.size(); k++) {    
            for (int i = 0; i < number_of_reccomends; i++) {
                this.users.get(k).reccomended_books.add(new book());
            }
        }
        BufferedReader reader = new BufferedReader(new FileReader(books_csv));
        reader.readLine();
        String line; 
        while((line = reader.readLine()) != null){
            String[] fields = line.split(";");
            for (int k = 0; k < this.users.size(); k++) {
                String[] book_words = (fields[1] + " " + fields[2] + " " + fields[4]).replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase().split(" ");
                Double book_score = (double)0;
                for (int i = 0; i < book_words.length; i++) {
                    Double feature_score = this.users.get(k).user_profile.get(book_words[i]);
                    if(feature_score != null) { 
                        book_score += feature_score; }
                }
                int lowest_index = this.users.get(k).get_lowest_element_index();
                if(this.users.get(k).reccomended_books.get(lowest_index).final_rating < book_score){
                    book temp_book = new book(fields[0]); temp_book.final_rating = book_score;
                    temp_book.title = fields[1]; temp_book.author = fields[2];
                    if(temp_book.final_rating > maximum_final_rating) maximum_final_rating = temp_book.final_rating;
                    if(temp_book.final_rating < minimum_final_rating) minimum_final_rating = temp_book.final_rating;
                    this.users.get(k).reccomended_books.set(lowest_index, temp_book);
                }
            }
        }
        this.normalize_all_final_ratings();
    }
    
    public void calculate_rmses() throws Exception{
        BufferedReader reader = new BufferedReader(new FileReader(books_csv));
        reader.readLine();
        String line; 
        while((line = reader.readLine()) != null){
            String[] fields = line.split(";");
            for (int i = 0; i < this.hidden_ratings.size(); i++) {
                for (int j = 0; j < this.users.size(); j++) {
                    if(fields[0].replace("\"", "").equals(hidden_ratings.get(i)[1].replace("\"", "")) && this.users.get(j).id == Integer.parseInt(this.hidden_ratings.get(i)[0].replace("\"", ""))){
                        double real_score = Double.parseDouble(this.hidden_ratings.get(i)[2].replace("\"", ""));
                        String[] book_words = (fields[1] + " " + fields[2] + " " + fields[4]).replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase().split(" ");
                        Double book_score = (double)0;
                        for (int k = 0; k < book_words.length; k++) {
                            Double feature_score = this.users.get(j).user_profile.get(book_words[k]);
                            if(feature_score != null) { 
                                book_score += feature_score; }
                        }
                    double predicted_score = normalize_final_rating(book_score);
                    this.rmse += Math.pow(real_score - predicted_score, 2);
                    }
                }
            }
        }
        this.rmse = Math.sqrt(this.rmse / this.hidden_ratings.size());
    }
    
    public void create_users_list(int number_of_users) throws Exception{
        BufferedReader reader = new BufferedReader(new FileReader(ratings_csv));
        reader.readLine(); // this is to skip first header line
        String line; 
        String previous = null;
        int counter = 0;
        ArrayList<String[]> same_user_lines = new ArrayList<>();
        ArrayList<String[]> useful_rows = new ArrayList<>();
        while((line = reader.readLine()) != null){
            String[] fields = line.split(";");
            if(fields[0].equals(previous)){
                same_user_lines.add(fields);
            } else {
                if(same_user_lines.size() > 5 && number_of_zeros(same_user_lines) < 2){
                    useful_rows.addAll(same_user_lines);
                    counter++;
                }
                same_user_lines = new ArrayList<>();
            }
            previous = fields[0];
            if(counter > number_of_users) break;
        }
        
        
        HashSet<String> already_added = new HashSet<>();
        
        for (int i = 0; i < useful_rows.size(); i++) {
            if(!already_added.contains(useful_rows.get(i)[0])){
                String a = useful_rows.get(i)[0];
                this.users.add(new user(useful_rows.get(i)[0]));
                already_added.add(useful_rows.get(i)[0]);
            }
            
            if((i + 4) % 5 == 0 && Integer.parseInt(useful_rows.get(i)[2].replace("\"", "")) > 0) { hidden_ratings.add(useful_rows.get(i)); }
        }
    }
    
    public int number_of_zeros(ArrayList<String[]> same_user_lines){
        int ret = 0;
        for (int i = 0; i < same_user_lines.size(); i++) {
            if(Integer.parseInt((same_user_lines.get(i)[2].replace("\"", ""))) == 0){ ret ++; }
        }
        return ret;
    }
    
    public double normalize_final_rating(double final_score){
        return (final_score - minimum_final_rating) / (maximum_final_rating - minimum_final_rating) * 10;
    }
    
    public void normalize_all_final_ratings(){
        for (int i = 0; i < this.users.size(); i++) {
            for (int j = 0; j < this.users.get(i).reccomended_books.size(); j++) {
                this.users.get(i).reccomended_books.get(j).final_rating = normalize_final_rating(this.users.get(i).reccomended_books.get(j).final_rating);
            }
        }
    }

    private void add_new_user_ratings_to_csv() throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(ratings_csv,true));
        BufferedReader reader = new BufferedReader(new FileReader(this.new_user_ratings_csv));
        String line; reader.readLine();
        while((line = reader.readLine()) != null){
            writer.append(line);
            writer.newLine();
        }
        writer.close();
    }

    private void write_recommends_in_csv() throws Exception {
        BufferedWriter writer = new BufferedWriter(new FileWriter(recommendeds_csv,true));
        writer.append("User id: " + this.users.get(0).id);
        writer.newLine();
        for (int i = 0; i < this.users.get(0).reccomended_books.size(); i++) {
            writer.append("\"" + this.users.get(0).reccomended_books.get(i).isbn + "\";\"" + this.users.get(0).reccomended_books.get(i).title+ "\";\"" + this.users.get(0).reccomended_books.get(i).author+ "\";\"");
            writer.newLine();
        }
        writer.close();
    }
}