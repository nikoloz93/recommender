package reccomender.objects;

import java.util.*;

public class user {
    public int id;
    public String location;
    public int age;
    public ArrayList<book> books;
    public HashMap<String, Integer> isbn_score;
    public HashMap<String, Double> user_profile;
    public ArrayList<book> reccomended_books = new ArrayList<book>();
    
    
    public user(String id){
        this.id = Integer.parseInt(this.remove_extra_chars(id));
        this.isbn_score = new HashMap<String, Integer>();
        this.books = new ArrayList<book>();
    }
    
    public String remove_extra_chars(String string){
        string = string.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase();
        return string;
    }

    public String index_of_feature(String feature){
        String key = "";
        for (Map.Entry<String, Double> entry : this.user_profile.entrySet()) {
            if(entry.getKey() == feature){
                key = entry.getKey();
            }
        }
        return key;
    }
    
    public int get_lowest_element_index() {
        double smallest = this.reccomended_books.get(0).final_rating;
        int index = 0;
        for (int i = 0; i < this.reccomended_books.size(); i++) {
            if(this.reccomended_books.get(i).final_rating < smallest){
                index = i;
            }
        }
        return index;
    }
}