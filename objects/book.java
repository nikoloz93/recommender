package reccomender.objects;

import java.util.*;

public class book {
    public String isbn;
    public String title;
    public String author;
    public String publisher;
    public int publication_year;
    public int user_score;
    public String profile_items;
    public ArrayList<Integer> maximum_appearences = new ArrayList<>();
    public ArrayList<Integer> total_appearences = new ArrayList<>();
    public ArrayList<Integer> in_document_appearence = new ArrayList<>();
    public ArrayList<Double> tf_idfs = new ArrayList<>();
    public double final_rating = 0;
    
    public book(String isbn, String title, String author, String publication_year, String publisher, int user_score){
        this.isbn = this.remove_extra_chars(isbn);
        this.title = this.remove_extra_chars(title);
        this.author = this.remove_extra_chars(author);
        this.publisher = this.remove_extra_chars(publisher);
        this.publication_year = Integer.parseInt(this.remove_extra_chars(publication_year));
        this.user_score = user_score;
    }
    
    public book(){
        
    }
    
    public book(String isbn){
        this.isbn = this.remove_extra_chars(isbn);
    }
    
    public String remove_extra_chars(String string){
        string = string.replaceAll("[^a-zA-Z0-9 ]", "").toLowerCase().trim();
        return string;
    }
    
    public void generate_profile_items(){
        this.profile_items = this.title + " " + this.author + " " + this.publisher + " " + this.publication_year;
    }
    
    public void fill_appearance_placeholders(){
        for (int i = 0; i < this.profile_items.split(" ").length; i++) {
            this.maximum_appearences.add(0);
            this.total_appearences.add(0);
            this.in_document_appearence.add(0);
        }
    }
}