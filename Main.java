package reccomender;

import reccomender.objects.result;

public class Main {
    
    public static void main(String[] args) {
        reccomender recommender = new reccomender();
        result result = recommender.find_recommended_books();
        if(result.error_occured){
            System.out.println(result.error_long_message);
        } else {
            System.out.println("Recommended books successfully written in recommended_books.csv");
        }
    }
    
}